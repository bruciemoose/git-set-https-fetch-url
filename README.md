# git-set-https-fetch-url

This simple plugin is useful if you have an onerous process like pressing a
Yubikey when you push, and don't want to go through it when you pull.

## Installation

Put `git-set-https-fetch-url` in your PATH

## Usage

```
$ git clone some@gitfab.biz/mywriteable/repo
$ cd repo
$ git set-https-fetch-url origin
remotes before:
origin  some@gitfab.biz:mywriteable/repo (fetch)
origin  some@gitfab.biz:mywriteable/repo (push)

remotes after:
origin  https://gitfab.biz/mywriteable/repo (fetch)
origin  some@gitfab.biz:mywriteable/repo (push)
$
```
